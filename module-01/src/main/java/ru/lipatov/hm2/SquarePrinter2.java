package ru.lipatov.hm2;

import java.util.Scanner;

public class SquarePrinter2 {

    public static void printSquare(int countRow) {
        int diag1 = 0;
        int diag2 = countRow-1;
        VERT: for (int i = 0; i < countRow; i++) {

            HORIZ: for (int j = 0; j < countRow; j++) {

                if (i == Math.floor(countRow/2)) {
                    diag1++;
                    diag2--;
                    continue VERT;
                }

                if (j == Math.floor(countRow/2)) { continue; }
                if (( j == diag1 ) || ( j == diag2 )) {
                    System.out.print("0");
                } else if (j > Math.max(diag1, diag2)) {
                    System.out.print("/");

                } else if (j< Math.min(diag1, diag2)) {
                    System.out.print("*");
                } else System.out.print("+");
            }
            System.out.println();
            diag1++;
            diag2--;
        }
    }


    public static int askSquareCollumns(){
        int squareColumns;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Введите нечетное количество клеток");
            squareColumns = scanner.nextInt();
            if (squareColumns % 2 == 0) {
                System.out.println("Ошибка, Вы ввели четное число!");
            }
        } while (squareColumns % 2 == 0);
        return squareColumns;
    }
}
