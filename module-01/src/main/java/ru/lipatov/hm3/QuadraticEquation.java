package ru.lipatov.hm3;

public class QuadraticEquation {
    public static void calculateAndPrint(double a, double b, double c) {
        double d = b * b - 4 * a * c;
        if ( d<0 || a == 0 || b ==0 ) {
            System.out.println("Уравнение не имеет решений!!!");
        } else if (d > 0) {
            double x1, x2;
            x1 = (-b - Math.sqrt(d)) / (2 * a);
            x2 = (-b + Math.sqrt(d)) / (2 * a);
            System.out.println("Корни уравнения: x1 = " + x1 + ", x2 = " + x2);
        } else if (d == 0) {
            double x;
            x = -b / (2 * a);
            System.out.println("Уравнение имеет единственный корень: x = " + x);
        }

    }


}
