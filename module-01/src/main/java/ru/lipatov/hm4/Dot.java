package ru.lipatov.hm4;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Dot {
    private int x;
    private int y;

    public double findDistanceTo (Dot toDot){
        return sqrt(pow(toDot.y - y, 2) + pow(toDot.x - x, 2));
    }

    public Dot(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
