package ru.lipatov.hm4;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        int x1, x2, y1, y2;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите X для первой точки");
        x1 = scanner.nextInt();
        System.out.println("Введите Y для первой точки");
        y1 = scanner.nextInt();

        Dot firstDot = new Dot(x1, y1);

        System.out.println("Введите X для второй точки");
        x2 = scanner.nextInt();
        System.out.println("Введите Y для второй точки");
        y2 = scanner.nextInt();

        Dot secondDot = new Dot(x2, y2);

        System.out.println();

        double whatDistance = firstDot.findDistanceTo(secondDot);
        System.out.println("расстояние между точками: " + whatDistance);
    }
}
